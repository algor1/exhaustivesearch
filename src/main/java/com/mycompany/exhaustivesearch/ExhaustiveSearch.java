/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.exhaustivesearch;

/**
 *
 * @author ASUS
 */
public class ExhaustiveSearch {
    static boolean process(int arr[]){
        int sum = 0;
        int n = arr.length;
        for(int i = 0 ; i < arr.length ; i++){
            sum+= arr[i];
        } 
//        if(sum % 2 == 1){
//            return false;
//        }
        
        int H = sum/2;
        
        int count = 1;
        int sum2 = 0;
        int x = equalPartition(n,arr);
        if(x == 1){
            System.out.println("Yes");
        }else{
            System.out.println("No");
        }
        return true;
    }
    
    static int equalPartition(int N, int arr[])
    {
        int sum = 0;
        for(int i = 0;i < N;i++)
            sum = sum + arr[i];
        if(sum%2 == 1)
            return 0;
        sum = sum/2;
        boolean[][] dp =new boolean[N+1][sum+1];
	    
	    for(int i= 0;i<dp.length;i++)
	        dp[i][0] = true;
	    
	    for(int i= 1;i<dp.length;i++)
	        dp[0][i] = false;
	     
	    for(int i = 1;i <= N;i++){
	        for(int j = 1;j <= sum;j++){
	            if(arr[i-1] > j){
	                dp[i][j] = dp[i-1][j];
	            }
	            else{
	                dp[i][j] = dp[i-1][j] || dp[i-1][j-arr[i-1]];
	            }
	        }
	    }
	    return (dp[N][sum]?1:0);
    }
    

    public static void main(String[] args) {
        int arr[]={1,2,4,4,5,6,10,8,9};
        for(int i = 0 ; i < arr.length ; i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println("");
        process(arr);
    }
}
